The plugin "OpenDoors" has been tested with V2.5.3 of PTP on Linux.

Plugin description:
This plugin allows to change parts of your pano by displaying distorted hotspots (Open-/Closed-Door-effect).

The values needed for calculating the hotspots distorsion settings have to be looked up manually before you can use this plugin.

You need to know the following values for each distorted hotspot image you want to incorprate into your panos:
HFOV (Horizontal field of view), Yaw, Pitch and Roll.

There is a detailed explanation/tutorial in the KRPano-Forum, have a look at this link:
http://krpano.com/forum/wbb/index.php?page=Thread&threadID=7272

Usage:
For every distorted hotspot you want to use add an additional group of settings via the configuration menu.
Fill in the required values: distorted hotspot image path, HFOV, Yaw,Pitch and Roll.
Remember the index number PTP assigns to every group of settings, starting with "0". This index number can be given as the required parameter to the show-, hide- or toggle-actions for the [Plugin] OpenDoors action selector.

That way you can change between different states by assigning the desired action to a hotspot or button.
